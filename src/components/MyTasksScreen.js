import React from 'react';
import { Button, View, ScrollView, Text } from 'react-native';
import TodoItem from './TodoItem';
import {connect} from 'react-redux';

class MyTasksScreen extends React.Component {

    static navigationOptions = ({navigation}) => ({
        headerTitle: 'My Tasks',
        headerRight: <View style = {{paddingRight: 15}}><Button title = 'Add Task' color = 'black' onPress = {() => {navigation.navigate('AddTodo')}}/></View>,
    })

    constructor(props){
        super(props);
        this.state = {
            taskName: [],
        };
    }

    renderTodoItem(taskName) {
        const arrayLength = this.state.taskName.length;
        let index = 0;
        let newArray = [];
        for(index; index < arrayLength; index++){
            newArray.push(
                <View key={index} style={{flex: 1}}>
                    <TodoItem taskName = {taskName[index]}/>
                </View>
            )
        }
        return (
              newArray  
        );
    }
    
    render() {

        const {navigation} = this.props;
        this.state.taskName = [...this.state.taskName, navigation.getParam('taskName', 'Add Task')];

        return (
            <ScrollView>
                {this.renderTodoItem(this.state.taskName)}
            </ScrollView>
        );
    }
}

export default MyTasksScreen;