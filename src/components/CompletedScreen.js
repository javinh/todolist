import React from 'react';
import { Button, View, ScrollView, } from 'react-native';
import TodoItem from './TodoItem';

class CompletedScreen extends React.Component {

    static navigationOptions = {
        headerTitle: 'My Tasks',
    }
      
    constructor(props){
        super(props)
        this.state = {taskName: ''}
    }

    renderTodoItem(taskName) {
        if(taskName !== '') {
            return (
                <View flex = {1}>
                    <TodoItem taskName = {taskName}/>
                </View>
            );
        }
    }
    
    render() {
        return (
        <ScrollView>

        </ScrollView>
        );
    }
}

// function mapStateToProps(state) {
//     return {
//         taskName: state.taskName,
//     };
// }
  
export default CompletedScreen;