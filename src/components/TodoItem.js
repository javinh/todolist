import React from 'react';
import { Text, } from 'react-native';
import Swipeout from 'react-native-swipeout';

class TodoItem extends React.Component {

    render() {

        var swipeoutBtnsRight = [
            {
              text: 'Delete',
              backgroundColor: 'red',
            },
        ]
    
          var swipeoutBtnsLeft = [
            {
              text: 'Complete',
              backgroundColor: 'green',
            },
        ]

        return(
            <Swipeout left = {swipeoutBtnsLeft} right = {swipeoutBtnsRight} autoClose = {true}>
                <Text style = {{borderColor: 'black', borderWidth: 1, fontSize: 40}}>{this.props.taskName}</Text>
            </Swipeout>
        );
    }
}

export default TodoItem;