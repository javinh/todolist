import React from 'react';
import {View, Text, Dimensions} from 'react-native';
import PieChart from 'react-native-pie-chart';

class InsightsScreen extends React.Component {

    wh = Dimensions.get('window').width - 50;

    render() {
        return (
            <View flex = {1} flexDirection = {'column'}>

                <View flex = {1.5} flexDirection = {'row'}>

                    <View flex = {1} flexDirection = {'column'} backgroundColor = {'#C591CB'} alignItems = {'center'} justifyContent = {'center'}>
                        <Text style = {{color: 'white', fontSize: 40, fontWeight: 'bold'}}>3</Text>
                        <Text style = {{color: 'black', fontSize: 15, fontWeight: 'bold'}}>To Do</Text>
                    </View>

                    <View flex = {1} flexDirection = {'column'} backgroundColor = {'#39CE8D'} alignItems = {'center'} justifyContent = {'center'}>
                        <Text style = {{color: 'white', fontSize: 40, fontWeight: 'bold'}}>7</Text>
                        <Text style = {{color: 'black', fontSize: 15, fontWeight: 'bold'}}>Completed</Text>
                    </View>

                </View>

                <View flex = {1} flexDirection = {'row'} justifyContent = {'center'} alignItems = {'center'}>
                    <Text style = {{color: 'gray', }}>You Currently Have </Text>
                    <Text style = {{fontWeight: 'bold'}}>10</Text>
                    <Text style = {{color: 'gray', }}> Tasks</Text>
                </View>

                <View flex = {5} flexDirection = {'row'} justifyContent = {'center'}>
                    <PieChart
                        chart_wh = {this.wh}
                        series = {[7, 3]}
                        sliceColor = {['#39CE8D','#C591CB']}
                        doughnut = {true}
                        coverRadius = {0.85}
                        coverFill = {'white'}
                    />
                </View>

            </View>
        );
    }
}

export default InsightsScreen;