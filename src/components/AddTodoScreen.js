import React from 'react';
import { Text, View, TextInput, TouchableOpacity } from 'react-native';
import {connect} from 'react-redux';
import store from '../store';
import { addTask } from '../actions/actions';

class AddTodoScreen extends React.Component {

    static navigationOptions = {
      headerTitle: 'Add Task'
    }

    constructor(props){
      super(props);
      this.state = {
        taskName: '',
      }
    }
  
    render(){
  
      return(
        <View style = {{flex: 1, flexDirection: 'column'}}>
          
          <Text style = {{padding: 10, paddingTop: 20, paddingLeft: 15, fontWeight: 'bold', fontSize: 25}}>
            Your Task:
          </Text>
  
          <View style = {{paddingHorizontal: 15}}>
            <TextInput
              style = {{height: 40}}
              placeholder = "Enter Your Task Here..."
              placeholderTextColor = 'silver'
              borderColor = 'silver'
              borderWidth = {1}
              underlineColorAndroid = 'transparent'
              paddingHorizontal = {10}
              onChangeText = {(taskName) => 
                this.setState({taskName})}
              onChangeText = {(taskName) => 
                this.state.taskName = {taskName}}
            />
          </View>
  
          <View style = {{flexDirection: 'row-reverse', paddingVertical: 10, paddingHorizontal: 15}}>
            <TouchableOpacity
              style = {{backgroundColor: 'cyan', borderRadius: 8, height: 20*3, width: 60*3, alignItems: 'center', justifyContent: 'center'}}
              onPress = {() =>
                this.props.navigation.navigate('MyTasks', this.state.taskName)
              }
            >
              <Text style = {{color: 'white', fontSize: 20, fontWeight: 'bold'}}>
                Add Task
              </Text>
            </TouchableOpacity>
          </View>
        
        </View>
      );
    }
}

export default AddTodoScreen;