import MyTasksScreen from './MyTasksScreen';
import AddTodoScreen from './AddTodoScreen';
import InsightsScreen from './InsightsScreen';
import {createStackNavigator, createBottomTabNavigator, createMaterialTopTabNavigator} from 'react-navigation';
import CompletedScreen from './CompletedScreen';

const TodoCompletedStack = createMaterialTopTabNavigator(
    {
        Todo: MyTasksScreen,
        Completed: CompletedScreen,
    },
    {
        initialRouteName: 'Todo',
    }
);

const TodoStack = createStackNavigator(
    {
        MyTasks: MyTasksScreen,
        AddTodo: AddTodoScreen,
    },
    {
        initialRouteName: 'MyTasks',
  
        navigationOptions: {
            headerStyle: {backgroundColor: 'cyan'},
            headerTintColor: 'white',
            headerTintStyle: {fontWeight: 'bold'},
        },
    },
);

const InsightsStack = createStackNavigator(
    {
        Insights: InsightsScreen,
    },
    {
        initialRouteName: 'Insights',

        navigationOptions: {
            headerStyle: {backgroundColor: 'cyan'},
            headerTintColor: 'white',
            headerTintStyle: {fontWeight: 'bold'},
            title: 'Insights',
        },
    },
);

const RootStack = createBottomTabNavigator (
    {
        'My Tasks': TodoStack,
        Insights: InsightsStack,
    },
    {
        tabBarOptions: {
            activeBackgroundColor: 'black',
            activeTintColor: 'cyan',
            inactiveTintColor: 'gray',
            inactiveBackgroundColor: 'black',
        },
    }
);

export default RootStack;