import React from 'react';
import RootStack from './TodoNavigator';

class TodoApp extends React.Component {
    render() {
        return(
            <RootStack/>
        );
    }
}

export default TodoApp;