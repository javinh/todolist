import tasks from './reducers/reducers';
import { createStore } from 'redux';

const store = createStore(tasks);

export default store;