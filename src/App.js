import React from 'react';
import TodoApp from './components/TodoApp';
import { Provider } from 'react-redux';
import store from './store';

export default class App extends React.Component {
  render() {
    return (
      <Provider store = {store}>
        <TodoApp/>
      </Provider>
    );
  }
}