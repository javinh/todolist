let nextTodoID = 0;

export const addTask = (text) => ({
    type: 'ADD_TASK',
    id: nextTodoID++,
    payload: text,
});

export const deleteTask = (index) => ({
    type: 'DELETE_TASK',
    payload: index,
});

export const markCompleted = (index) => ({
    type: 'MARK_COMPLETED',
    payload: index,
});