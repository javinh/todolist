const tasks = (state = [], action) => {

  switch(action.type) {

    case 'ADD_TASK':
      return [...state, {id: action.id, text: action.payload, completed: false}];
    
    case 'DELETE_TASK':
      return [...state, state.splice(action.payload, 1)];
    
    case 'MARK_COMPLETED':
      return [...state, state.indexOf(action.payload).completed = true];
    
    default:
      return state;
  }
}

export default tasks;